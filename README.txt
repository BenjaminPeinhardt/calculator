Sup Team 13. These are some practice files that I'm throwing in a repository in case any wants to practice with git.

There are three files: Main.java, Dataset.java, and Testscript.java
Please feel free to add methods to the Dataset class or make new class structures for other math stuff, and try the simultaneous testing with asserts in the Testscript class. Obviously these are very simple, its for git practice not java practice, but if you wanna go ham on a method for a regression equation, you do you. 