package calculator;

import java.lang.Math;

public class Testscript {
    public static void main(String[] args) {
        // Gonna Test DataSet here.
        double data[] = {1,1,2,3,5,8,13,21};
        Dataset testData = new Dataset(data);

        // Testing sum method
        assert testData.getTotal() == 54 : "getTotal() fail";

        // Testing val method
        for (int i = 0; i < data.length; i++) {
            assert testData.getVal(i) == data[i] : "getVal() fail";
        }

        // Testing mean method
        assert Math.abs(testData.getMean() - 6.75) <= 0.01 : "getMean() fail";

        // Testing StdDev
        assert Math.abs(testData.getStdDev() - 7.066) <= 0.001 : "getStdDev() fail";

        // Testing z-score
        assert Math.abs(testData.getZScore(7) - 0.03538) <= 0.001 : "getZScore() fail";
    }
}