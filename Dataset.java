package calculator;
import java.lang.Math;

public class Dataset {
    private final double values[];

    // Constructor
    public Dataset(final double arr[]) {
        this.values = arr;
    }

    // Methods
    public double getVal(final int i) {
        return this.values[i];
    }

    public double getTotal() {
        double sum = 0;
        for (int i = 0; i < this.values.length; i++) {
            sum += this.values[i];
        }
        return sum;
    }

    public double getMean() {
        double sum = getTotal();
        sum /= (this.values.length);
        return sum;
    }

    public double getMedian() {
        int i1, i2;
        if (this.values.length % 2 == 0) {
            i1 = (this.values.length / 2) - 1;
            i2 = (this.values.length / 2);

            return (this.values[i1] + this.values[i2]) / 2;
        } else {
            i1 = this.values.length / 2;

            return this.values[i1];
        }
    }

    public double getStdDev() {
        double diffs[];
        diffs = new double[this.values.length];

        final double mean = getMean();
        double sum = 0;

        for (int i = 0; i < this.values.length; i++) {
            diffs[i] = this.values[i] - mean;
            diffs[i] *= diffs[i];
            sum += diffs[i];
        }

        sum /= (this.values.length - 1);
        return Math.sqrt(sum);
    }

    // Z-score added on z-score branch
    public double getZScore(final double el) {
        return (el - this.getMean())/this.getStdDev();
    }
}